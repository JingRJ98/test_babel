// 可以看到babel插件导出一个函数，函数里面return一个对象，其中的visitor属性也是最核心
// babel 在使用 @babel/traverse 对 AST 进行深度遍历时，会 访问 每个 AST 节点，
// 这个便是跟我们的 visitor 有关。babel 会在 访问 AST 节点的时候，调用 visitor  中对应节点类型的方法，这便是 babel 插件暴露给开发者的核心。


// path.remove() 节点删除
// path.get(key) 获取当前路径下的子孙节点
// path.replaceWith(newNode) (单)节点替换函数

module.exports = function () {
  return {
    visitor: {
      CallExpression(path) {
        // 对源代码中每个函数调用表达式进行判断
        const call = path.get('callee')
        const obj = call.get('object')

        if(obj.node.name === 'console' && call){
          path.remove()
        }
      }
    }
  }
}