/** 
 * 实现babel插件:普通的console.log()函数中插入当前的文件名和行列号, 便于快速定位
 * console.log(1)
 * 转译为
 * console.log('文件名 (行号 列号): ',1)
 */


console.log(1)
console.log(null)
console.log('')
console.log(undefined)
console.log(() => {
  const a = 1
  console.log(a)
})