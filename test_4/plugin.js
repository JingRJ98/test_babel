const targetCalleeName = ['log', 'info', 'error', 'debug'].map(item => `console.${item}`);
module.exports = function ({ types }) {
  return {
    visitor: {
      // 要改什么就传入这个type的函数
      // 此处要修改log函数
      CallExpression(path, state) {
        const callee = path.get('callee')
        const calleeName = `${callee.get('object')}.${callee.get('property')}`
        if (targetCalleeName.includes(calleeName)) {
          const { line, column } = path.node.loc.start
          const newNode = types.stringLiteral(`file: ${state.filename}, lien: ${line}, column: ${column}`)
          // 直接改数组
          path.node.arguments.unshift(newNode)
        }
      }
    }
  }
};
