/** 
 * 实现babel插件:普通的console.log()函数中插入当前的文件名和行列号, 便于快速定位
 * console.log(1)
 * 转译为
 * console.log('文件名 (行号 列号): ',1)
 */

console.log("file: /Users/jingrenjie/extra/test-babel/test_4/source.js, lien: 9, column: 0", 1);
console.log("file: /Users/jingrenjie/extra/test-babel/test_4/source.js, lien: 10, column: 0", null);
console.log("file: /Users/jingrenjie/extra/test-babel/test_4/source.js, lien: 11, column: 0", '');
console.log("file: /Users/jingrenjie/extra/test-babel/test_4/source.js, lien: 12, column: 0", undefined);
console.log("file: /Users/jingrenjie/extra/test-babel/test_4/source.js, lien: 13, column: 0", () => {
  const a = 1;
  console.log("file: /Users/jingrenjie/extra/test-babel/test_4/source.js, lien: 15, column: 2", a);
});
