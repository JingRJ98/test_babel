# 尝试写一个babel插件

## 编写插件流程
定义一个.babelrc文件用于配置插件
需要在`.babelrc`中进行插件的引入
```js
{
  "plugins": ["./test_x/plugin"]
}
```

插件代码
```js
// ./test_x/plugin.js
module.export = function() {
  return {
    vistor: {...}
  }
}
```

package.json中新增运行babel的命令
```js
{
  "scripts": {
    "example_x": "babel ./test_x/source.js --out-file ./test_x/build.js "
  },
}
```

原始文件路径 `./test_x/source.js`
转译后的文件路径 `./test_x/build.js`

## 本项目实现的插件

### test_1
修改变量的值

### test_2
删除代码中的console

### test_3
将?.可选链式运算符降级编译

`const val = a?.b`
转换成
`const val = a === null || a === void 0 ? void 0 : a.b`

### test_4
封装console.log()函数
实现babel插件:普通的console.log()函数中插入当前的文件名和行列号, 便于快速定位
console.log(1)
转译为
console.log('文件名 (行号 列号): ',1)