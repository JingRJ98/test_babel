// 可以看到babel插件导出一个函数，函数里面return一个对象，其中的visitor属性也是最核心
// babel 在使用 @babel/traverse 对 AST 进行深度遍历时，会 访问 每个 AST 节点，
// 这个便是跟我们的 visitor 有关。babel 会在 访问 AST 节点的时候，调用 visitor  中对应节点类型的方法，这便是 babel 插件暴露给开发者的核心。


// path.remove() 节点删除
// path.get(key) 获取当前路径下的子孙节点
// path.replaceWith(newNode) (单)节点替换函数

// const t = require('@babel/types')
// t可以用来创建AST节点
// 创建三元表达式 示例 4 > 3 ? 4 : 3
// t.conditionalExpression(
//   t.binaryExpression(
//     '>',
//     t.numericLiteral(4),
//     t.numericLiteral(3)
//   ),
//   t.numericLiteral(4),
//   t.numericLiteral(3)
// )

// 创建逻辑表达式 示例 num || 0
// t.logicalExpression(
//   '||',
//   t.identifier('num'),
//   t.numericLiteral(0)
// )

// 创建二元表达式 示例 1+2
// t.binaryExpression(
//   '+', // 操作符。 还支持：==, ===, != 等
//   item1, // 左操作数
//   item2
// )

// 创建获取对象的属性 示例 obj.name
// t.memberExpression(
//   t.identifier('obj'),
//   t.identifier('name')
// )

// 创建null
// t.nullLiteral()

const t = require('@babel/types')

module.exports = function () {
  // 应当将节点转换为一个三元表达式
  const transCondition = (node) => t.conditionalExpression(
    t.logicalExpression(
      '||',
      t.binaryExpression(
        '===',
        node.object,
        t.nullLiteral()
      ),
      t.binaryExpression(
        '===',
        node.object,
        t.unaryExpression('void', t.numericLiteral(0)),
      ),
    ),
    t.unaryExpression('void', t.numericLiteral(0)),
    // t.memberExpression(node.object, node.property)
    // 或者读取.name的字符串来创建 变量节点
    t.memberExpression(t.identifier(node.object.name), t.identifier(node.property.name))
  )

  return {
    visitor: {
      OptionalMemberExpression(path) {
        path.replaceWith(transCondition(path.node))
      }
    }
  }
}