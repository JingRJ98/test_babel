/**
 * 实现babel插件:可选链操作符转译成es的形式
 * const val = a?.b
 * 转换成
 * const val = a === null || a === void 0 ? void 0 : a.b
 */


const val = a?.b